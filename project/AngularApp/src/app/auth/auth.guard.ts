import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../shared/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  /**
   *
   */
  constructor(private router: Router, private service: UserService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (localStorage.getItem('token') != null) {
      let permittedRoles = next.data['permittedRoles'] as Array<string>;
      console.info(permittedRoles);
      if (permittedRoles) {
        if (this.service.roleMatch(permittedRoles)) return true;
        else {
          this.router.navigate(['/user/login']);
          return false;
        }
      }
      return true;
    }
    else {
      this.router.navigate(['/user/gestion']);
      return false;
    }
  }
}