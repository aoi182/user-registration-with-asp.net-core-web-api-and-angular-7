import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  readonly baseURL = 'http://localhost:5757/api';

  constructor(private fb: FormBuilder, private http: HttpClient) { }

  public formModel = this.fb.group({
    UserName: ['', Validators.required],
    Email: ['', Validators.email],
    FullName: [''],
    Passwords: this.fb.group({
      Password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['', Validators.required],
    }, {
      validator: this.comparePasswords
    }),
  });

  comparePasswords(fb: FormGroup) {
    let confirmPwdCtrl = fb.get('ConfirmPassword');
    if (confirmPwdCtrl.errors == null || 'passwordMissmatch' in confirmPwdCtrl.errors) {
      if (fb.get('Password').value != confirmPwdCtrl.value)
        confirmPwdCtrl.setErrors({ passwordMissmatch: true });
      else
        confirmPwdCtrl.setErrors(null);
    }
  }

  register() {
    var body = {
      UserName: this.formModel.value.UserName,
      Email: this.formModel.value.Email,
      FullName: this.formModel.value.FullName,
      Password: this.formModel.value.Passwords.Password
    }

    return this.http.post(this.baseURL + '/ApplicationUser/Register', body);
  }

  login(formData) {
    return this.http.post(this.baseURL + '/ApplicationUser/Login', formData);
  }

  getUserprofile() {
    return this.http.get(this.baseURL + '/UserProfile');
  }

  roleMatch(allowedRoles): boolean {
    var isMatch = false;
    var payLoad = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    console.info(payLoad);
    var userRoles = payLoad.role;
    if (!userRoles) return false;

    let intersection = allowedRoles.filter(x => userRoles.includes(x));
    
    if (intersection) return true; 
    else return false;
  }
}